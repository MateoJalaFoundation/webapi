﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Places.Models;

namespace Places.Data.Configuration
{
    public class PlaceConfig : IEntityTypeConfiguration<Place>
    {
        public void Configure(EntityTypeBuilder<Place> builder)
        {
            //Map the entities
            builder.ToTable("Places");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Country)
               .IsRequired();

            builder.Property(e => e.City)
                .IsRequired();

            builder.Property(e => e.Address)
                .IsRequired();
        }
    }
}
