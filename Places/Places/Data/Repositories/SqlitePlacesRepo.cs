﻿using Microsoft.EntityFrameworkCore;
using Places.Data.Interfaces;
using Places.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Places.Data.Repositories
{
    public class SqlitePlacesRepo : IPlacesRepo
    {
        private readonly PlaceContext _context;

        //Dependencies Injection
        public SqlitePlacesRepo(PlaceContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Place>> GetPlaces()
        {
            var places = await _context.Events.ToListAsync();
            return places;
        }

        public async Task<Place> GetPlacesById(int id)
        {
            var place = await _context.Events.FirstOrDefaultAsync(x => x.Id == id);
            return place;
        }
    }
}
