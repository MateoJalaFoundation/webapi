﻿using Microsoft.EntityFrameworkCore;
using Places.Data.Configuration;
using Places.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Places.Data
{
    public class PlaceContext :DbContext
    {
        public PlaceContext()
        {

        }

        public PlaceContext(DbContextOptions<PlaceContext> options) : base(options)
        {

        }

        public DbSet<Place> Events { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Builder for any table
            modelBuilder.ApplyConfiguration(new PlaceConfig());
        }
    }
}
