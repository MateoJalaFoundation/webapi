﻿using Places.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Places.Data.Interfaces
{
    public interface IPlacesRepo
    {
        //Get All events
        Task<IEnumerable<Place>> GetPlaces();

        //Get event by Id
        Task<Place> GetPlacesById(int id);
    }
}
