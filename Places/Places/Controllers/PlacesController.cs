﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Places.Data.Interfaces;
using Places.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Places.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlacesController : ControllerBase
    {
        private readonly IPlacesRepo _repository;
        private readonly IMapper _mapper;

        public PlacesController(IPlacesRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        //Define Functions 
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var placeItems = await _repository.GetPlaces();
            var placeItemsDto = _mapper.Map<IEnumerable<PlacesReadDto>>(placeItems);
            return Ok(placeItemsDto);
        }


        //Get by Id
        [HttpGet("{id}", Name = "GetById")]
        public async Task<ActionResult> GetById(int id)
        {
            var placeItem = await _repository.GetPlacesById(id);
            var placeItemDto = _mapper.Map<PlacesReadDto>(placeItem);
            if (placeItem != null)
            {
                return Ok(placeItemDto);
            }
            return NotFound();
        }
    }
}
