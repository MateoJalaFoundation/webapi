﻿using AutoMapper;
using Places.Dtos;
using Places.Models;

namespace Places.Profiles
{
    public class PlacesProfile :Profile
    {
        public PlacesProfile()
        {
            CreateMap<Place, PlacesReadDto>();
        }
    }
}
