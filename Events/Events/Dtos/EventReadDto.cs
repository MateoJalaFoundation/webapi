﻿using System;

namespace Events.Dtos
{
    public class EventReadDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
    }
}
