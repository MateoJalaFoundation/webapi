﻿using Events.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Events.Data.Interfaces
{
    public interface IEventsRepo
    {
        //Get All events
        Task<IEnumerable<Event>> GetEvents();

        //Get event by Id
        Task<Event> GetEventById(int id);
    }
}
