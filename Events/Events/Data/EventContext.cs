﻿using Events.Data.Configuration;
using Events.Models;
using Microsoft.EntityFrameworkCore;

namespace Events.Data
{
    public class EventContext : DbContext
    {
        public EventContext()
        {

        }

        public EventContext(DbContextOptions<EventContext> options) : base(options)
        {

        }

        public DbSet<Event> Events { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Builder for any table
            modelBuilder.ApplyConfiguration(new EventConfig());
        }
    }
}
