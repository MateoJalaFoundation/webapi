﻿using Events.Data.Interfaces;
using Events.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Events.Data.Repositories
{
    public class SqliteEventsRepo : IEventsRepo
    {
        private readonly EventContext _context;

        //Dependencies Injection
        public SqliteEventsRepo(EventContext context)
        {
            _context = context;
        }

        public async Task<Event> GetEventById(int id)
        {
            var evnt = await _context.Events.FirstOrDefaultAsync(x => x.Id == id);
            return evnt;
        }

        public async Task<IEnumerable<Event>> GetEvents()
        {
            var events = await _context.Events.ToListAsync();
            return events;
        }
    }
}
