﻿using AutoMapper;
using Events.Dtos;
using Events.Models;

namespace Events.Profiles
{
    public class EventsProfile:Profile
    {
        public EventsProfile()
        {
            CreateMap<Event, EventReadDto>();
        }
    }
}
