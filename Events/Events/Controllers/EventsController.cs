﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Events.Data.Interfaces;
using Events.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace Events.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventsRepo _repository;
        private readonly IMapper _mapper;

        public EventsController(IEventsRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        //Define Functions 
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var eventItems = await _repository.GetEvents();
            var eventItemsDto = _mapper.Map<IEnumerable<EventReadDto>>(eventItems);
            return Ok(eventItemsDto);
        }


        //Get by Id
        [HttpGet("{id}", Name = "GetById")]
        public async Task<ActionResult> GetById(int id)
        {
            var eventItem = await _repository.GetEventById(id);
            var eventItemDto = _mapper.Map<EventReadDto>(eventItem);
            if (eventItem != null)
            {
                return Ok(eventItemDto);
            }
            return NotFound();
        }
    }
}
