﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Organizers.Data.Interfaces;
using Organizers.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Organizers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizersController : ControllerBase
    {

        private readonly IOrganizerRepo _repository;
        private readonly IMapper _mapper;

        public OrganizersController(IOrganizerRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        //Define Functions 
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var organizerItems = await _repository.GetEvents();
            var organizerItemsDto = _mapper.Map<IEnumerable<OrganizerReadDto>>(organizerItems);
            return Ok(organizerItemsDto);
        }


        //Get by Id
        [HttpGet("{id}", Name = "GetById")]
        public async Task<ActionResult> GetById(int id)
        {
            var organizerItem = await _repository.GetEventById(id);
            var organizerItemDto = _mapper.Map<OrganizerReadDto>(organizerItem);
            if (organizerItem != null)
            {
                return Ok(organizerItemDto);
            }
            return NotFound();
        }
    }
}
