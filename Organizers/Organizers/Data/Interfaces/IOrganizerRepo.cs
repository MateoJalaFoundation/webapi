﻿using Organizers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Organizers.Data.Interfaces
{
    public interface IOrganizerRepo
    {
        //Get All events
        Task<IEnumerable<Organizer>> GetEvents();

        //Get event by Id
        Task<Organizer> GetEventById(int id);
    }
}
