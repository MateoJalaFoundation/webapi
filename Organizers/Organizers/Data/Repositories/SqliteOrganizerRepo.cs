﻿using Microsoft.EntityFrameworkCore;
using Organizers.Data.Interfaces;
using Organizers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Organizers.Data.Repositories
{
    public class SqliteOrganizerRepo :IOrganizerRepo
    {
        private readonly OrganizersContext _context;

        //Dependencies Injection
        public SqliteOrganizerRepo(OrganizersContext context)
        {
            _context = context;
        }

        public async Task<Organizer> GetEventById(int id)
        {
            var organizer = await _context.Organizers.FirstOrDefaultAsync(x => x.Id == id);
            return organizer;
        }

        public async Task<IEnumerable<Organizer>> GetEvents()
        {
            var organizers = await _context.Organizers.ToListAsync();
            return organizers;
        }
    }
}
