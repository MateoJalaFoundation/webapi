﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Organizers.Models;

namespace Organizers.Data.Configuration
{
    public class OrganizerConfig :IEntityTypeConfiguration<Organizer>
    {
        public void Configure(EntityTypeBuilder<Organizer> builder)
        {
            //Map the entities
            builder.ToTable("Organizers");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Name)
               .IsRequired();

            builder.Property(e => e.Nationality)
                .IsRequired();

            builder.Property(e => e.Position)
                .IsRequired();
        }
    }
}
