﻿using Microsoft.EntityFrameworkCore;
using Organizers.Data.Configuration;
using Organizers.Models;

namespace Organizers.Data
{
    public class OrganizersContext: DbContext
    {
        public OrganizersContext()
        {

        }
        public OrganizersContext(DbContextOptions<OrganizersContext> options) : base(options)
        {

        }

        public DbSet<Organizer> Organizers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Builder for any table
            modelBuilder.ApplyConfiguration(new OrganizerConfig());
        }
    }
}
