﻿using AutoMapper;
using Organizers.Dtos;
using Organizers.Models;

namespace Organizers.Profiles
{
    public class OrganizersProfile :Profile
    {
        public OrganizersProfile()
        {
            CreateMap<Organizer, OrganizerReadDto>();
        }
    }
}
