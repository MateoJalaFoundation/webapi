﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Organizers.Dtos
{
    public class OrganizerReadDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string nationality { get; set; }
        public string position { get; set; }
    }
}
